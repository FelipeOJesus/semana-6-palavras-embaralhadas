import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class BancoDePalavras {
	
	public List<String> buscaPalavrasNoArquivo() {

			List<String> listaDePalavras = new ArrayList<>();
			File caminho = new File(System.getProperty("user.dir") + "\\arquivos\\listaDePalavras.txt");
			try {
				FileReader arquivos = new FileReader(caminho);
				BufferedReader lerArquivo = new BufferedReader(arquivos);
				
				String linha = lerArquivo.readLine();
				while(linha != null) {
					listaDePalavras.add(linha);
					linha = lerArquivo.readLine();
				}

				lerArquivo.close();
				
			} catch (FileNotFoundException e) {
				System.out.println("Erro ao tentar abrir o arquivo \n" + e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Erro ao tentar ler dentro do arquivo \n" + e.getMessage());
				e.printStackTrace();
			}
			return listaDePalavras;
	}
	
	public String escolherPalavraAleatoriamente() {
		Random random = new Random();
		return buscaPalavrasNoArquivo().get(random.nextInt(buscaPalavrasNoArquivo().size())).toString();
	}
}
