
public interface Embaralhador {
	
	public String embaralharPalavraRandom(String palavra);
	
	public String embaralharPalavraDeTrasPraFrente(String palavra);

}
