import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FabricaEmbaralhadores implements Embaralhador{

	@Override
	public String embaralharPalavraRandom(String palavra) {
		List<String> letras = Arrays.asList(palavra.split(""));
		Collections.shuffle(letras);
		StringBuilder palavraEmbaralhada = new StringBuilder(palavra.length());
		
		for(String iterator : letras) {
			palavraEmbaralhada.append(iterator);
		}
		return palavraEmbaralhada.toString();
	}

	@Override
	public String embaralharPalavraDeTrasPraFrente(String palavra) {
		StringBuilder palavraEmbaralhada = new StringBuilder(palavra); 
		return palavraEmbaralhada.reverse().toString();
	}
	

}
