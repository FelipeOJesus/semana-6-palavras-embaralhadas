import java.util.ArrayList;
import java.util.List;

public class FabricaMecanicaDoJogo implements MecanicaDoJogo {

	List<String> listaDePalavrasCorretas = new ArrayList<>();
	int quantidadeDePontos;
	BancoDePalavras bancoDePalavras = new BancoDePalavras();
	
	private String palavraEscolhidaDoArquivo = null;
	
	private static int pontuacao = 0;
	private int erros = 0;

	@Override
	public int erro() {
		erros += 1;
		System.out.println("Resposta Errada");
		System.out.println("Chances restantes: " + (3 - erros));
		return erros;
	}

	@Override
	public boolean fimDoJogo() {
		if (getErros() < 3 || listaDePalavrasCorretas.size() == bancoDePalavras.buscaPalavrasNoArquivo().size())
			return false;
		return true;
	}

	@Override
	public boolean respostaDoUsuario(String resposta, String palavra) {
		if (palavra.equals(resposta)) {
			if (getPontuacao() >= 5)
				pontuacao(1);

			pontuacao(1);

			return true;
		} else {
			erro();
			return false;
		}

	}

	@Override
	public int pontuacao(int pontos) {
		pontuacao += pontos;
		return pontuacao;
	}

	public boolean vencedor() {
		if (getErros() < 3)
			return true;
		return false;
	}

	@Override
	public String tipoDeEmbaralhador(int pontuacaoNivel) {
		FabricaEmbaralhadores embaralhadores = new FabricaEmbaralhadores();
		palavraEscolhidaDoArquivo = bancoDePalavras.escolherPalavraAleatoriamente();
		if (pontuacaoNivel < 5) {
			return embaralhadores.embaralharPalavraDeTrasPraFrente(palavraEscolhidaDoArquivo);
		} else {
			return embaralhadores.embaralharPalavraRandom(palavraEscolhidaDoArquivo);
		}
	}

	public int getPontuacao() {
		return pontuacao;
	}

	public int getErros() {
		return erros;
	}

	public void zerarPontuacao() {
		pontuacao = 0;
		erros = 0;
	}

	public String getPalavraEscolhidaDoArquivo(){
		return palavraEscolhidaDoArquivo;
	}

}
