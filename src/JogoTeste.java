import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class JogoTeste {

	FabricaMecanicaDoJogo mecanismoDoJogo;
	BancoDePalavras bancoDePalavras;
	FabricaEmbaralhadores embaralhadores;

	@Before
	public void inicializador() {
		mecanismoDoJogo = new FabricaMecanicaDoJogo();
		mecanismoDoJogo.zerarPontuacao();
		bancoDePalavras = new BancoDePalavras();
		embaralhadores = new FabricaEmbaralhadores();
	}

	@Test
	public void respostaCertaDoUsuario() {
		String palavraEscolhida = mecanismoDoJogo.tipoDeEmbaralhador(mecanismoDoJogo.getPontuacao());
		assertTrue(mecanismoDoJogo.respostaDoUsuario(palavraEscolhida, palavraEscolhida));
	}

	@Test()
	public void respostaErradaDoUsuario() {
		String palavraEscolhida = mecanismoDoJogo.tipoDeEmbaralhador(mecanismoDoJogo.getPontuacao());

		assertFalse(mecanismoDoJogo.respostaDoUsuario("Errado", palavraEscolhida));
	}
	
	@Test
	public void contabilizarErros() {
		mecanismoDoJogo.erro();
		mecanismoDoJogo.erro();
		
		assertEquals(2, mecanismoDoJogo.getErros());
	}

	@Test
	public void pontuacaoDificuldadeUm() {
		String palavraEscolhida = mecanismoDoJogo.tipoDeEmbaralhador(mecanismoDoJogo.getPontuacao());

		mecanismoDoJogo.respostaDoUsuario(palavraEscolhida, palavraEscolhida);
		mecanismoDoJogo.respostaDoUsuario(palavraEscolhida, palavraEscolhida);
		mecanismoDoJogo.respostaDoUsuario(palavraEscolhida, palavraEscolhida);

		assertEquals(3, mecanismoDoJogo.getPontuacao());
	}

	@Test
	public void pontuacaoDificuldadeDois() {

		String palavraEscolhida = mecanismoDoJogo.tipoDeEmbaralhador(mecanismoDoJogo.getPontuacao());

		mecanismoDoJogo.respostaDoUsuario(palavraEscolhida, palavraEscolhida);
		mecanismoDoJogo.respostaDoUsuario(palavraEscolhida, palavraEscolhida);
		mecanismoDoJogo.respostaDoUsuario(palavraEscolhida, palavraEscolhida);
		mecanismoDoJogo.respostaDoUsuario(palavraEscolhida, palavraEscolhida);
		mecanismoDoJogo.respostaDoUsuario(palavraEscolhida, palavraEscolhida);
		mecanismoDoJogo.respostaDoUsuario(palavraEscolhida, palavraEscolhida);
		mecanismoDoJogo.respostaDoUsuario(palavraEscolhida, palavraEscolhida);

		assertEquals(9, mecanismoDoJogo.getPontuacao());

	}

	@Test
	public void embaralharNivelUm() {
		String palavraEscolhida = bancoDePalavras.escolherPalavraAleatoriamente();

		assertNotEquals(palavraEscolhida, embaralhadores.embaralharPalavraDeTrasPraFrente(palavraEscolhida));
	}

	@Test
	public void embaralharNivelDois() {
		String palavraEscolhida = bancoDePalavras.escolherPalavraAleatoriamente();

		assertNotEquals(palavraEscolhida, embaralhadores.embaralharPalavraRandom(palavraEscolhida));
	}

	@Test
	public void fimDoJogo() {
		mecanismoDoJogo.erro();
		mecanismoDoJogo.erro();
		mecanismoDoJogo.erro();
		
		assertTrue(mecanismoDoJogo.fimDoJogo());
	}

}
