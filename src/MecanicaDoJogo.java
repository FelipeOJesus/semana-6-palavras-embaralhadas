public interface MecanicaDoJogo {

	public int erro();
	
	public boolean fimDoJogo();
	
	public boolean respostaDoUsuario(String resposta, String palavra);
	
	public int pontuacao(int pontos);
	
	public String tipoDeEmbaralhador(int randomico);

}
