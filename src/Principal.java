
import java.util.Scanner;
import java.util.logging.Logger;

public class Principal {

	public static void main(String[] args) {
		FabricaMecanicaDoJogo mecanismoDoJogo = new FabricaMecanicaDoJogo();

		//Instancia da palavra que será digitada pelo usuário
		Scanner palavraDigitadaPeloUsuario = new Scanner(System.in);

		while (!mecanismoDoJogo.fimDoJogo()) {
			String palavraEmbaralhada = mecanismoDoJogo.tipoDeEmbaralhador(mecanismoDoJogo.getPontuacao());

			System.out.println("");
			System.out.println("Palavra Embaralhada: " + palavraEmbaralhada);
			System.out.print("Digite a palavra corretamente: ");

			mecanismoDoJogo.respostaDoUsuario(palavraDigitadaPeloUsuario.nextLine(), mecanismoDoJogo.getPalavraEscolhidaDoArquivo());				
		}

		System.out.println();
		System.out.println("Pontua��o Final: " + mecanismoDoJogo.getPontuacao());
	}

}
